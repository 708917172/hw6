#include "Unit.h"
#include "Ast.h"
#include "SymbolTable.h"
#include "Type.h"
#include<string>
extern FILE* yyout;

void Unit::insertFunc(Function *f)
{
    func_list.push_back(f);
}

void Unit::insertGlobal(IdentifierSymbolEntry*se)
{
    global_list.push_back(se);
}

void Unit::removeFunc(Function *func)
{
    func_list.erase(std::find(func_list.begin(), func_list.end(), func));
}

void Unit::output() const
{
    for(auto &se:global_list)
    {
        fprintf(yyout, "%s = global %s %d, align 4\n", se->toStr().c_str(),se->getType()->toStr().c_str(),se->getValue());
    }
    for (auto &func : func_list)
        func->output();

        
    SymbolEntry* se1;
    SymbolEntry* se2;
    SymbolEntry* se3;
    SymbolEntry* se4;
    std::string s1("getint");
    std::string s2("putint");
    std::string s3("getch");
    std::string s4("putch");
    se1=identifiers->lookup(s1);
    se2=identifiers->lookup(s2);
    se3=identifiers->lookup(s3);
    se4=identifiers->lookup(s4);
    if(se1) fprintf(yyout, "%s\n","declare i32 @getint()");
    if(se2) fprintf(yyout, "%s\n","declare void @putint(i32)");
    if(se3) fprintf(yyout, "%s\n","declare i32 @getch()");
    if(se4) fprintf(yyout, "%s\n","declare void @putch(i32)");
}

Unit::~Unit()
{
    for (auto& se : global_list)
        delete se;
}
