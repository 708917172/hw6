%code top{
    #include <iostream>
    #include <assert.h>
    #include "parser.h"
    extern Ast ast;
    int yylex();
    int yyerror( char const * );
    int temp=0;
    int temp2=0;
}

%code requires {
    #include "Ast.h"
    #include "SymbolTable.h"
    #include "Type.h"
}

%union {
    int itype;
    char* strtype;
    StmtNode* stmttype;
    ExprNode* exprtype;
    Type* type;
}

%start Program
%token <strtype> ID 
%token <itype> INTEGER
%token IF ELSE
%token INT VOID
%token LPAREN RPAREN LBRACE RBRACE SEMICOLON COMMA CONST WHILE
%token ADD SUB MUL DIV MOD OR AND LESS ASSIGN EQ UEQ LESSASS GREA GREAASS NOT
%token RETURN

%nterm <stmttype> Stmts Stmt AssignStmt BlockStmt IfStmt WhileStmt ReturnStmt DeclStmt VarDeclStmt VarListStmt VarStmt ConstDeclStmt ConstList ConstStmt FuncDef FuncPa OFuncPas FuncPas ExprStmt 
%nterm <exprtype> Exp MulExp AddExp Cond LOrExp PrimaryExp LVal RelExp LAndExp UnaryExp FuncRParams
%nterm <type> Type

%precedence THEN
%precedence ELSE
%%
Program
    : Stmts {
        ast.setRoot($1);
    }
    ;
Stmts
    : Stmt {$$=$1;}
    | Stmts Stmt{
        $$ = new SeqNode($1, $2);
    }
    ;
Stmt
    : AssignStmt {$$=$1;}
    | BlockStmt {$$=$1;}
    | IfStmt {$$=$1;}
    | WhileStmt {$$=$1;}
    | ReturnStmt {$$=$1;}
    | DeclStmt {$$=$1;}
    | FuncDef {$$=$1;}
    | ExprStmt{$$=$1;}
    ;
LVal
    : ID {
        SymbolEntry *se;
        se = identifiers->lookup($1);

        if(!se) {
			printf("%s\n","Not definition");
		}

        $$ = new Id(se);
        delete []$1;
    }
    ;
AssignStmt
    :
    LVal ASSIGN Exp SEMICOLON {
        $$ = new AssignStmt($1, $3);
    }
    ;
BlockStmt
    :   LBRACE 
        {identifiers = new SymbolTable(identifiers);} 
        Stmts RBRACE 
        {
            $$ = new CompoundStmt($3);
            SymbolTable *top = identifiers;
            identifiers = identifiers->getPrev();
            delete top;
        }
    | 
	SEMICOLON{
	$$=new CompoundStmt(NULL);
	}
    |
	LBRACE RBRACE
    {
        $$= new CompoundStmt(NULL);
    }
    ;
IfStmt
    : IF LPAREN Cond RPAREN Stmt %prec THEN {
        $$ = new IfStmt($3, $5);
    }
    | IF LPAREN Cond RPAREN Stmt ELSE Stmt {
        $$ = new IfElseStmt($3, $5, $7);
    }
    ;
WhileStmt
    : WHILE LPAREN Cond RPAREN Stmt {
        $$ = new WhileStmt($3, $5);
    }
    ;
ReturnStmt
    :
    RETURN Exp SEMICOLON{
        $$ = new ReturnStmt($2);
    }
    |
    RETURN SEMICOLON {
        $$ = new ReturnStmt();
    }
    ;
ExprStmt
    : Exp SEMICOLON {
        $$ = new ExprStmt($1);
    }
    ;
Exp
    :
    AddExp {$$ = $1;}
    ;
Cond
    :
    LOrExp {$$ = $1;}
    ;
PrimaryExp
    :
    LVal {
        $$ = $1;
    }
    |
    LPAREN Exp RPAREN {
        $$ = $2;
    }
    | INTEGER {
        SymbolEntry *se = new ConstantSymbolEntry(TypeSystem::intType, $1);
        $$ = new Constant(se);
    }
    | ID LPAREN FuncRParams RPAREN {
        SymbolEntry* se1;
        Node *p = $3;
        while(p)
        {
            temp++;
            p = p->next;
        }
        se1 = identifiers->lookup($1);
        if(!se1) {
			printf("%s\n","Function not definition");
		}
        se1 = identifiers->lookup($1,temp);
        if(!se1) {
			printf("%s\n","Function Params error");
		}
        temp=0;


        SymbolEntry* se;
        se = identifiers->lookup($1);
        $$ = new CallExpr(se, $3);
    }
    | ID LPAREN RPAREN {
        SymbolEntry* se;
        se = identifiers->lookup($1);

        SymbolEntry* se1;
        se1= identifiers->lookup($1,0);
        if(!se1) {
			printf("%s\n","Function not definition");
		}
        if(se->c!=0) printf("%s","Number of Params error");

        $$ = new CallExpr(se);
    }
    ;
UnaryExp
	:
	PrimaryExp{
        $$ = $1;
    }
	|
    ADD UnaryExp
	{
	    $$ = $2;
	}
	|
	NOT UnaryExp
	{
	    SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
        $$ = new UnaryExp(se, UnaryExp::NOT, $2);
	}
	|
	SUB UnaryExp
	{
	    SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        $$ = new UnaryExp(se, UnaryExp::SUB, $2);
	}
	;
MulExp
	:UnaryExp {$$ = $1;}
    |
    MulExp MUL UnaryExp{
		SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        $$ = new BinaryExpr(se, BinaryExpr::MUL, $1, $3);
	}
	|
	MulExp DIV UnaryExp{
		SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        $$ = new BinaryExpr(se, BinaryExpr::DIV, $1, $3);
	}
	|
	MulExp MOD UnaryExp{
		SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        $$ = new BinaryExpr(se, BinaryExpr::MOD, $1, $3);
	}
	;
AddExp
    :
    AddExp ADD MulExp
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        $$ = new BinaryExpr(se, BinaryExpr::ADD, $1, $3);
    }
    |
    AddExp SUB MulExp
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        $$ = new BinaryExpr(se, BinaryExpr::SUB, $1, $3);
    }
    |MulExp{$$=$1;}
    ;
RelExp
    :
    AddExp {$$ = $1;}
    |
    RelExp LESS AddExp
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
        $$ = new BinaryExpr(se, BinaryExpr::LESS, $1, $3);
    }
    |RelExp LESSASS AddExp
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
        $$ = new BinaryExpr(se, BinaryExpr::LESSASS, $1, $3);
    }
    |RelExp GREA AddExp
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
        $$ = new BinaryExpr(se, BinaryExpr::GREA, $1, $3);
    }
    |RelExp GREAASS AddExp
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
        $$ = new BinaryExpr(se, BinaryExpr::GREAASS, $1, $3);
    }
    |RelExp EQ AddExp
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
        $$ = new BinaryExpr(se, BinaryExpr::EQ, $1, $3);
    }
    |RelExp UEQ AddExp
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
        $$ = new BinaryExpr(se, BinaryExpr::UEQ, $1, $3);
    }
    ;
FuncRParams 
    : Exp {$$ = $1;}
    | FuncRParams COMMA Exp {
        $$ = $1;
        $$->setNext($3);
    }
LAndExp
    :
    RelExp {$$ = $1;}
    |
    LAndExp AND RelExp
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
        $$ = new BinaryExpr(se, BinaryExpr::AND, $1, $3);
    }
    ;
LOrExp
    :
    LAndExp {$$ = $1;}
    |
    LOrExp OR LAndExp
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
        $$ = new BinaryExpr(se, BinaryExpr::OR, $1, $3);
    }
    ;
Type
    : INT {
        $$ = TypeSystem::intType;
    }
    | VOID {
        $$ = TypeSystem::voidType;
    }
    ;
DeclStmt
    : VarDeclStmt {$$ = $1;}
    |ConstDeclStmt{$$=$1;}
    ;
ConstDeclStmt
    : CONST Type ConstList SEMICOLON{$$=$3;}
    ;
ConstList
    :ConstList COMMA ConstStmt{
        $$=$1;
        $$->setNext($3);
    }
    |ConstStmt{$$=$1;}
    ;
ConstStmt
    : ID {
        SymbolEntry* se;

        se = identifiers->lookupscope($1);
		if(se){
			printf("%s\n","Duplicate definition");
		}


        se = new IdentifierSymbolEntry(TypeSystem::intType, $1, identifiers->getLevel());
        identifiers->install($1, se);
        $$ = new DeclStmt(new Id(se));
    }
    |ID ASSIGN Exp{
        SymbolEntry* se;

        se = identifiers->lookupscope($1);
		if(se){
			printf("%s\n","Duplicate definition");
		}

        se = new IdentifierSymbolEntry(TypeSystem::intType, $1, identifiers->getLevel());
        identifiers->install($1, se);
		$$ = new DeclStmt(new Id(se),$3);
    }
    ;
VarDeclStmt
    : Type VarListStmt SEMICOLON {$$ = $2;}
    ;
VarListStmt
    : VarListStmt COMMA VarStmt {
        $$ = $1;
        $1->setNext($3);
    } 
    | VarStmt {$$ = $1;}
    ;
VarStmt
    : ID {
        SymbolEntry* se;


        se = identifiers->lookupscope($1);
		if(se){
			printf("%s\n","Duplicate definition");
		}


        se = new IdentifierSymbolEntry(TypeSystem::intType, $1, identifiers->getLevel());
        identifiers->install($1, se);
        $$ = new DeclStmt(new Id(se));
    }
    |ID ASSIGN Exp{
        SymbolEntry* se;

        se = identifiers->lookupscope($1);
		if(se){
			printf("%s\n","Duplicate definition");
		}

        se = new IdentifierSymbolEntry(TypeSystem::intType, $1, identifiers->getLevel());
        identifiers->install($1, se);
		$$ = new DeclStmt(new Id(se),$3);
    }
    ;
FuncDef
    :
    Type ID {


        
        SymbolEntry *se;
        se = identifiers->lookup($2);
        if(se) temp2=se->c+1;



        identifiers = new SymbolTable(identifiers);
    }
    LPAREN OFuncPas RPAREN
    {
        Type *funcType;
        std::vector<Type*> paramtype;
        std::vector<SymbolEntry*> param;
        DeclStmt* temp = (DeclStmt*)$5;
        while(temp)
        {
            paramtype.push_back(temp->getId()->getSymPtr()->getType());
            param.push_back(temp->getId()->getSymPtr());
            temp = (DeclStmt*)(temp->getNext());
        }
        funcType = new FunctionType($1,paramtype,param);
        SymbolEntry *se = new IdentifierSymbolEntry(funcType, $2, identifiers->getPrev()->getLevel());
        identifiers->getPrev()->install($2, se);
    }
    BlockStmt
    {
        SymbolEntry *se;
        se = identifiers->lookup($2);


        Node *p = $5;
        while(p)
        {
            se->c++;
            p = p->next;
        }
        if(se->c==temp2-1&&temp2!=0) printf("%s","Func redef error\n");
        temp2=0;


        $$ = new FunctionDef(se, $8,$5);
        SymbolTable *top = identifiers;
        identifiers = identifiers->getPrev();
        delete top;
        delete []$2;
    }
    ;
OFuncPas
	:
	FuncPas{$$=$1;};
	| %empty {$$ = NULL;}
FuncPas
	:
	FuncPas COMMA FuncPa{
		$$ = $1;
        $$->setNext($3);
	}
	|
	FuncPa{$$=$1;}
	;
FuncPa:
	Type ID{ 
	SymbolEntry* se;
    se = new IdentifierSymbolEntry(TypeSystem::intType, $2, identifiers->getLevel());
    identifiers->install($2, se);
    ((IdentifierSymbolEntry*)se)->setLabel(identifiers->getLabel());
    ((IdentifierSymbolEntry*)se)->setAddr(new Operand(se));
    $$ = new DeclStmt(new Id(se),NULL);
    delete []$2;}
	|
	Type ID ASSIGN Exp{
		SymbolEntry* se;
        se = new IdentifierSymbolEntry(TypeSystem::intType, $2, identifiers->getLevel());
        identifiers->install($2, se);
		delete []$1;
		$$ = new DeclStmt(new Id(se), $4);
	}
	;
%%

int yyerror(char const* message)
{
    std::cerr<<message<<std::endl;
    return -1;
}
